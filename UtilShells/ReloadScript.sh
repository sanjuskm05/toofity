# Script to delete cache and reinstall the modules

watchman watch-del-all
rm -rf node_modules && npm install
npm start -- -- reset-cache
