import React from "react";
import { createStackNavigator, createDrawerNavigator, DrawerItems } from "react-navigation";
import { StyleSheet, SafeAreaView, ScrollView, Image, View , Dimensions } from 'react-native';
import { DrawerActions } from 'react-navigation';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';

import Main from "./Main";
import SaveAddress from "./SaveAddress";
import ConfirmAddress from "./ConfirmAddress";
import PaymentOptions from "./PaymentOptions";
import ProductList from "./ProductList";
import ProductPage from "./ProductPage";
import Login from "./Login";
import SignUp from "./SignUp";
import Profile from "./Profile";
import Shop from "./Shop";
import Product from "./Product";
import HomePage from "./HomePage";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class App extends React.Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}

const CustomDrawerComponent = (props) => (
  <SafeAreaView style={styles.container}>
    <View style={ styles.drawerCover }>
      <Image source={require('../../images/screenForSideBar.png')}
              style={ styles.drawerImage }/>
    </View>
    <ScrollView>
      <DrawerItems {...props}/>
    </ScrollView>
  </SafeAreaView>
)

const Drawer = createDrawerNavigator({
    Home : Main,
    SaveAddress: SaveAddress,
    ConfirmAddress: ConfirmAddress,
    PaymentOptions: PaymentOptions,
    //ProductList: ProductList,
    //ProductPage: ProductPage,
    Login: Login,
    SignUp: SignUp,
    Profile: Profile,
    //Shop: Shop,
    //Product: Product
  }, {
    contentComponent: CustomDrawerComponent,
    contentOptions : {
      activeTintColor: 'orange'
    }
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer },
    Main: { screen: Main },
    SaveAddress: { screen: SaveAddress },
    ConfirmAddress: { screen: ConfirmAddress },
    PaymentOptions: { screen: PaymentOptions },
    ProductList: { screen: ProductList },
    ProductPage: { screen: ProductPage },
    Login: { screen: Login },
    SignUp: { screen: SignUp },
    Profile: { screen: Profile },
    Shop: { screen: Shop },
    Product: { screen: Product }
  },
  {
    index: 0,
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  drawerCover: {
    height: deviceHeight / 5.5,
    position: "relative",
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerImage: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  }
});