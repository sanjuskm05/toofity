import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  List,
  ListItem,
  InputGroup,
  Input,
  Icon,
  Card,
  CardItem,
  Body,
  Grid,
  Col
} from "native-base";
import { observer, inject } from "mobx-react/native";
import ListDropdown from "../CommonComponents/ListDropdown/index.js";
import RoundImageButton from "../CommonComponents/RoundImageButton/index.js";
import BannerSlider from "../CommonComponents/BannerSlider/index.js";
import Banner from "../CommonComponents/Banner/index.js";
import ThemeHeader from "../CommonComponents/Header/index.js";
import MyFooter from "../CommonComponents/Footer";
import commonColor from "../../theme/variables/commonColor.js";
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import Style from "./style.js";
var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

var bannerSliderData = [
  {
    id: 1,
    bannerImageSource: require("../../images/dewalt-angle-grinders-dcg412b-64_400_compressed.jpg"),
    bannerImageText: "Dewalt grinders",
    bannerSmallText: "Angle grinder"
  },
  {
    id: 2,
    bannerImageSource: require("../../images/milwaukee-angle-grinders-2780-20-64_400_compressed.jpg"),
    bannerImageText: "Milwaukee grinders",
    bannerSmallText: "Angle grinder"
  },
  {
    id: 3,
    bannerImageSource: require("../../images/milwaukee-reciprocating-saws-2720-20-64_400_compressed.jpg"),
    bannerImageText: "Milwaukee saws",
    bannerSmallText: "Reciprocating saw"
  },
  {
    id: 4,
    bannerImageSource: require("../../images/milwaukee-table-saws-2736-21hd-64_400_compressed.jpg"),
    bannerImageText: "Milwaukee saws",
    bannerSmallText: "Table saw"
  }
];

@inject("view.app", "domain.user", "app", "routerActions")
@observer
class HomePage extends Component {
  render() {
    const userStore = this.props["domain.user"];
    const navigation = this.props.navigation;
    return (
      <Container>
        <ThemeHeader 
          PageTitle="ToolFity" 
          IconLeft="menu" 
          navigation={navigation}
          IconRight="search" 
          />
        <Content
          padder
          contentContainerStyle={{ paddingBottom: 20 }}
          showsVerticalScrollIndicator={false}
        >
          <Card style={Style.scrollBanner}>
            <Text style={Style.bannerHeading}>
              Choose yoour tool 
            </Text>
            <List
              removeClippedSubviews={false}
              directionalLockEnabled={false}
              horizontal={false}
              showsHorizontalScrollIndicator={false}
              bounces={false}
              dataArray={bannerSliderData}
              renderRow={item =>
                <BannerSlider
                  onPress={() => alert('Added to cart')}
                  bannerImageSource={item.bannerImageSource}
                  bannerImageText={item.bannerImageText}
                  bannerSmallText={item.bannerSmallText}
                />}
            />
          </Card>
        </Content>
        <MyFooter navigation={navigation} selected={"home"} />
      </Container>
    );
  }
}
export default HomePage;
