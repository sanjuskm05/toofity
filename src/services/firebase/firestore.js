import { firestore } from './firebase';

// User API

export const doCreateUser = (id, email, createTimestamp) =>
    firestore.collection('users').doc(`${id}`).set({
    email,
  });

export const onceGetUsers = () =>
    firestore.ref('users').once('value');