import { observable, action, computed } from 'mobx';

class UserStore {
  @observable username = 'DP';
  @action updateUserName = (newUsername) => {
    this.username = newUsername;
  }
//Fetch deviceID from device and initialize the app with user information
}

export default UserStore;
